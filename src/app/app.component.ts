import {Component} from '@angular/core';
import {Employee} from "./models/Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {BearerToken} from "./models/BearerToken";
import {LoginService} from "./services/login.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  bearer = '';
  title = 'Employee Management Service';
  loggedIn = false;

  constructor(
    private http: HttpClient,
    private loginService: LoginService
    ) {
  }



}
