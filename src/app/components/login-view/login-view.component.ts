import { Component, OnInit } from '@angular/core';
import {LoginService} from "../../services/login.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TokenHolderService} from "../../services/token-holder.service";

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css']
})
export class LoginViewComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };

  loginAttempts = 0;
  lockoutTime = 0;
  subscribeTimer: any;
  interval;
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';

  constructor(private authService: LoginService, private tokenHolder: TokenHolderService, private router: Router) { }

  ngOnInit() {
    if (this.tokenHolder.getToken()) {
      this.isLoggedIn = true;
      this.authService.setUserLoggedIn();
      this.router.navigate(['employees']);
    }
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.lockoutTime > 0) {
        this.lockoutTime--;
        this.errorMessage = 'Too many failed login attempts, locked out for ' + this.lockoutTime + ' second(s)';
      } else {
        this.loginAttempts = 0;
        this.errorMessage = '';
        this.isLoginFailed = false;
        clearInterval(this.interval);
      }
    },1000)
  }

  onSubmit() {
    if (this.loginAttempts < 3 && this.lockoutTime < 120) {
      const { username, password} = this.form;
      this.authService.login(username, password).subscribe({
        next: data => {
          this.tokenHolder.saveToken(data.access_token);
          this.isLoginFailed = false;
          this.isLoggedIn = true;
          this.authService.setUserLoggedIn();
          this.router.navigate(['employees']);
        },
        error: err => {
          this.loginAttempts++;
          this.lockoutTime += 40;
          console.log('Login attempts:' + this.loginAttempts)
          this.errorMessage = 'Invalid Credentials';
          this.isLoginFailed = true;
          this.authService.setUserLoggedOut();
        }
      })
    }else {
      this.startTimer();
      this.errorMessage = 'Too many failed login attempts, locked out for ' + this.lockoutTime + ' second(s)';
      this.isLoginFailed = true;
    }
  }

  reloadPage() {
    window.location.reload();
  }

  //TODO: Change the lockoutTime to be saved in localStorage to make it less easy to avoid the timer by just reloading the page
}
