import { Component, OnInit } from '@angular/core';
import { Employee } from "../../models/Employee";
import { EmployeeDataService } from "../../services/employee-data.service";
import {Qualification} from "../../models/Qualification";
import {QualificationFilterComponent} from "../qualification-filter/qualification-filter.component";
import {QualificationDataService} from "../../services/qualification-data.service";

@Component({
  selector: 'app-create-new-employee',
  templateUrl: './create-new-employee.component.html',
  styleUrls: ['./create-new-employee.component.css']
})
export class CreateNewEmployeeComponent implements OnInit {

  employee: Employee;
  messageErrorE: string = 'Please fill in this form (Postcode must have 5 caracter)';
  newQualification: boolean = false;

  constructor(private employeeDataService: EmployeeDataService, private qualificationDataService: QualificationDataService) {
    this.employee = new Employee(null);
  }

  ngOnInit(): void {
  }

  saveEmployee() {
    if (this.employee.firstName !== undefined && this.employee.lastName !== undefined && this.employee.street !== undefined && this.employee.postcode !== undefined
      && this.employee.city !== undefined && this.employee.phone !== undefined) {
      this.employeeDataService.addEmployee(this.employee).subscribe(
        item => { if (this.qualificationDataService.qualificationDesignation !== undefined
        && this.qualificationDataService.qualificationDesignation !== ''
        && this.qualificationDataService.qualificationDesignation !== 'Select a qualification') {
          this.qualificationDataService.qualification1.designation = this.qualificationDataService.qualificationDesignation;
          this.qualificationDataService.postEmployeeQualification(item.id).subscribe();
          this.qualificationDataService.qualificationDesignation = '';
          this.qualificationDataService.qualification1.designation = '';
        }

        });
      this.messageErrorE = 'New Employee is successfully created';
    }
  }

  getNewQualification(): boolean {
    return this.newQualification;
  }

  setNewQualification(): void {
    if(this.newQualification == false) {
      this.newQualification = true;
    }else {
      this.newQualification = false;
    }
  }

}
