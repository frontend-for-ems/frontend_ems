import { Component, OnInit } from '@angular/core';
import { Employee } from "../../models/Employee";
import { EmployeeDataService } from "../../services/employee-data.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees$: Observable<Employee[]>;

  selectedEmployees: Employee[] = [];

  constructor(private employeeDataService: EmployeeDataService) {
    this.employees$ = this.employeeDataService.employees$;
  }

  ngOnInit(): void {
  }

  addToSelectedList(employee: Employee) {
    const employees: Employee[] = this.selectedEmployees.filter(e => e!== employee);
    if(this.selectedEmployees.length == employees.length) {
      this.selectedEmployees.push(employee);
    } else {
      this.selectedEmployees = employees;
    }
  }

  deleteEmployee(employee: Employee) {
    this.employeeDataService.delete(employee);
    location.reload();
  }

  deleteSelectedEmployees() {
    for (let i = 0; i < this.selectedEmployees.length; i++) {
      this.deleteEmployee(this.selectedEmployees[i]);
    }
  }


}
