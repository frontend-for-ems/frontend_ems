import { Component, OnInit } from '@angular/core';
import { Qualification } from "../../models/Qualification";
import { QualificationDataService } from "../../services/qualification-data.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-qualification-filter',
  templateUrl: './qualification-filter.component.html',
  styleUrls: ['./qualification-filter.component.css']
})
export class QualificationFilterComponent implements OnInit {

  qualifications$: Observable<Qualification[]>;
  designation: string = '';

  constructor(private qualificationDataService: QualificationDataService) {
    this.qualifications$ = this.qualificationDataService.qualification$;
  }

  ngOnInit(): void {
  }

  setQualification(event: any) {
    this.designation = event.target.value;
    this.qualificationDataService.qualificationDesignation = event.target.value;
  }

}
