import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualificationFilterComponent } from './qualification-filter.component';

describe('QualificationFilterComponent', () => {
  let component: QualificationFilterComponent;
  let fixture: ComponentFixture<QualificationFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualificationFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QualificationFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
