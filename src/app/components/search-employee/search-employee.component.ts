import { Component, OnInit } from '@angular/core';
import { Employee } from "../../models/Employee";
import { EmployeeDataService } from "../../services/employee-data.service";


@Component({
  selector: 'app-search-employee',
  templateUrl: './search-employee.component.html',
  styleUrls: ['./search-employee.component.css']
})
  export class SearchEmployeeComponent implements OnInit {

  searchedEmployeeId: number;

  constructor(private employeeDataService: EmployeeDataService) { }

  ngOnInit(): void {
  }



}
