import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Employee } from "../../models/Employee";
import { EmployeeDataService } from "../../services/employee-data.service";
import {Qualification} from "../../models/Qualification";
import {Observable} from "rxjs";
import {EmployeeQualifications, SkillSet} from "../../models/EmployeeQualifications";
import {EmployeeQualificationsDataService} from "../../services/employee-qualifications-data.service";
import {HttpHeaders} from "@angular/common/http";


@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  employee: Employee;
  employeeQualifications: EmployeeQualifications;

  constructor(private employeeDataService: EmployeeDataService, private employeeQualificationsDataService: EmployeeQualificationsDataService,
              private route: ActivatedRoute,
              private location: Location) {
    this.employee = new Employee(null);
    this.employeeQualifications = new EmployeeQualifications(null);
  }

  ngOnInit(): void {
    this.getEmployee();
    this.getEmployeeQualification();
  }

  getEmployee(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.employeeDataService.getEmployeeById(id)
      .subscribe(employee => this.employee = employee);
  }

  getEmployeeQualification(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.employeeQualificationsDataService.getEmployeeQualifications(id)
      .subscribe(employeeQualifications => this.employeeQualifications = employeeQualifications);
  }

  deleteEmployee() {
    this.employeeDataService.delete(this.employee);
    location.replace('/employees');
  }

  updateEmployee(employee: Employee) {
    this.employeeDataService.putEmployee(employee).subscribe();
    location.replace('/employees');
  }


}
