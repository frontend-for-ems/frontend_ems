import { Component, OnInit } from '@angular/core';
import { Qualification } from "../../models/Qualification";
import {Observable, of} from "rxjs";
import { QualificationDataService } from "../../services/qualification-data.service";
import {Employee} from "../../models/Employee";

@Component({
  selector: 'app-qualification-list',
  templateUrl: './qualification-list.component.html',
  styleUrls: ['./qualification-list.component.css']
})
export class QualificationListComponent implements OnInit {

  qualifications$: Observable<Qualification[]>;

  selectedQualifications: Qualification[] = [];

  constructor(private qualificationDataService: QualificationDataService) {
    this.qualifications$ = this.qualificationDataService.qualification$;
  }


  ngOnInit(): void {
  }


  addToSelectedList(qualification: Qualification) {
    const qualifications: Qualification[] = this.selectedQualifications.filter(e => e!== qualification);
    if(this.selectedQualifications.length == qualifications.length) {
      this.selectedQualifications.push(qualification);
    } else {
      this.selectedQualifications = qualifications;
    }
  }

  deleteQualification(qualification: Qualification) {
    this.qualificationDataService.delete(qualification);
    location.reload();
  }

  deleteSelectedQualifications() {
    for (let i = 0; i < this.selectedQualifications.length; i++) {
      this.deleteQualification(this.selectedQualifications[i]);
    }
  }

}
