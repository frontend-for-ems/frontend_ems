import { Component, OnInit } from '@angular/core';
import { Qualification } from "../../models/Qualification";
import { QualificationDataService } from "../../services/qualification-data.service";

@Component({
  selector: 'app-create-qualification',
  templateUrl: './create-qualification.component.html',
  styleUrls: ['./create-qualification.component.css']
})
export class CreateQualificationComponent implements OnInit {

  qualification: Qualification;
  messageErrorQ: string = 'Enter a valid qualification';

  constructor(private qualificationDataService: QualificationDataService) {
    this.qualification = new Qualification(null);
  }

  ngOnInit(): void {
  }

  save() {
    if (this.qualification.designation !== null) {
      this.qualificationDataService.addQualification(this.qualification).subscribe();
      this.messageErrorQ = 'New qualification is successfully created';
    }
  }

}
