import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { LoginViewComponent } from './components/login-view/login-view.component';
import { AppRoutingModule } from './app-routing.module';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeDetailsComponent } from './components/employee-details/employee-details.component';
import { QualificationListComponent } from './components/qualification-list/qualification-list.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AlertComponent} from "./components/alert/alert.component";
import { CreateNewEmployeeComponent } from './components/create-new-employee/create-new-employee.component';
import { NavComponent } from './components/nav/nav.component';
import { SearchEmployeeComponent } from './components/search-employee/search-employee.component';
import { QualificationFilterComponent } from './components/qualification-filter/qualification-filter.component';
import { SearchQualificationComponent } from './components/search-qualification/search-qualification.component';
import { authInterceptorProviders } from './helpers/auth.interceptor';
import { CreateQualificationComponent } from './components/create-qualification/create-qualification.component';
import {AuthGuard} from "./helpers/auth.guard";

@NgModule({
  declarations: [
    AppComponent,
    LoginViewComponent,
    EmployeeListComponent,
    EmployeeDetailsComponent,
    QualificationListComponent,
    AlertComponent,
    QualificationListComponent,
    LoginViewComponent,
    CreateNewEmployeeComponent,
    QualificationListComponent,
    NavComponent,
    SearchEmployeeComponent,
    QualificationFilterComponent,
    SearchQualificationComponent,
    CreateQualificationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [authInterceptorProviders, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
