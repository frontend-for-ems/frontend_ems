import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeListComponent } from "./components/employee-list/employee-list.component";
import { LoginViewComponent } from "./components/login-view/login-view.component";
import { QualificationListComponent } from "./components/qualification-list/qualification-list.component";
import { EmployeeDetailsComponent } from "./components/employee-details/employee-details.component";
import { CreateQualificationComponent } from "./components/create-qualification/create-qualification.component";
import { CreateNewEmployeeComponent } from "./components/create-new-employee/create-new-employee.component";
import {AuthGuard} from "./helpers/auth.guard";

const routes: Routes = [
  { path: 'newEmployee', component: CreateNewEmployeeComponent, canActivate: [AuthGuard]},
  { path: 'newQualification', component: CreateQualificationComponent, canActivate: [AuthGuard]},
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'newEmployee', component: CreateNewEmployeeComponent, canActivate: [AuthGuard]},
  { path: 'login', component: LoginViewComponent },
  { path: 'employees', component: EmployeeListComponent, canActivate: [AuthGuard]},
  { path: 'qualifications', component: QualificationListComponent, canActivate: [AuthGuard]},
  { path: 'detail/:id', component: EmployeeDetailsComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
