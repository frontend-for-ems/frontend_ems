import { Injectable } from '@angular/core';
import { Qualification } from "../models/Qualification";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { LoginService } from "./login.service";
import {Employee} from "../models/Employee";

@Injectable({
  providedIn: 'root'
})
export class QualificationDataService {

  qualification$: Observable<Qualification[]>;
  qualificationDesignation: string = '';

  qualification1: Qualification = new Qualification( '');

  constructor(private http: HttpClient, private loginService: LoginService) {
    this.qualification$ = of([]);
    //this.employeeQualifications$ = of([]);
    //this.postEmployeeQualification().subscribe();
    this.fetchData();
    /*this.addQualification(this.qualification1).subscribe();
    this.addQualification(this.qualification2).subscribe();
    this.addQualification(this.qualification3).subscribe();*/
  }

  fetchData() {
    this.qualification$ = this.http.get<Qualification[]>('/backend/qualifications', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    });
  }

  addQualification(qualification: Qualification): Observable<Qualification> {
    return this.http.post<Qualification>('/backend/qualifications',qualification, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    });
  }

  delete(qualification: Qualification) {
    console.log('hallo');
    return this.http.delete('/backend/qualifications/', {
      body: qualification,
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    }).subscribe();
  }

  /*getEmployeeQualifications() {
    this.employeeQualifications$ = this.http.get<Employee[]>('/backend/employees/1/qualifications', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    });
  }
*/

  postEmployeeQualification(id: number): Observable<Qualification> {
    return this.http.post<Qualification>(`/backend/employees/${id}/qualifications`, this.qualification1 , {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    });
  }


}
