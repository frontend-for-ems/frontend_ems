import { Injectable } from '@angular/core';
import {BearerToken} from "../models/BearerToken";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {BehaviorSubject, map, Observable, of, TimeInterval} from "rxjs";
import {User} from "../models/User";
import {Router} from "@angular/router";
import {TokenHolderService} from "./token-holder.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded'
  })
};

const AUTH_API = 'http://authproxy.szut.dev';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn = false;

  constructor(private http: HttpClient, private router: Router, private tokenHolder: TokenHolderService) {
  }


  signIn(user: User) {
    let body = new URLSearchParams();
    body.set('grant_type', 'password');
    body.set('client_id', 'employee-management-service');
    if (typeof user.username === "string") {
      body.set('username', user.username);
    }
    if (typeof user.password === "string") {
      body.set('password', user.password);
    }
    console.log(body.toString());
    return this.http.post<any>(AUTH_API, body.toString(), httpOptions).subscribe((response: any) => {
      localStorage.setItem('access_token', response.access_token)
    });
  }

  login(username: string, password: string): Observable<any> {
    let body = new URLSearchParams();
    body.set('grant_type', 'password');
    body.set('client_id', 'employee-management-service');
    body.set('username', username);
    body.set('password', password);
    console.log(body.toString());
    return this.http.post(AUTH_API, body, httpOptions)
  }

  setUserLoggedIn() {
    this.isLoggedIn = true;
  }

  setUserLoggedOut() {
    this.isLoggedIn = false;
    this.tokenHolder.signOut();
  }

  getLoginStatus(): boolean {
    return this.isLoggedIn;
  }

  getToken(){
    if (localStorage.getItem('access_token')) {
      return localStorage.getItem('access_token');
    }
    else return '';
  }

}
