import { Injectable } from '@angular/core';
import { Employee } from "../models/Employee";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { LoginService } from "./login.service";
import { Qualification } from "../models/Qualification";

@Injectable({
  providedIn: 'root'
})
export class EmployeeDataService {

  employees$: Observable<Employee[]>;

  employeeTest1: Employee = new Employee(1, 'Amal', 'Müller', 'Mauerstraße', '2033ß', 'Erlangen', '+494758483939');
  employeeTest2: Employee = new Employee(1, 'Walid', 'Müller', 'Mauerstraße', '2033ß', 'Erlangen', '+494758483939');
  employeeTest3: Employee = new Employee(1, 'Insaf', 'Müller', 'Mauerstraße', '2033ß', 'Erlangen', '+494758483939');
  employeeTest4: Employee = new Employee(1, 'Debora', 'Müller', 'Mauerstraße', '2033ß', 'Erlangen', '+494758483939');
  employeeTest5: Employee = new Employee(1, 'Andi', 'Müller', 'Mauerstraße', '2033ß', 'Erlangen', '+494758483939');
  employeeTest6: Employee = new Employee(1, 'Nisrine', 'Müller', 'Mauerstraße', '2033ß', 'Erlangen', '+494758483939');

  constructor(private http: HttpClient, private loginService: LoginService) {
    this.employees$ = of([]);
    this.fetchData();
    /*this.addEmployee(this.employeeTest1).subscribe();
    this.addEmployee(this.employeeTest2).subscribe();
    this.addEmployee(this.employeeTest3).subscribe();
    this.addEmployee(this.employeeTest4).subscribe();
    this.addEmployee(this.employeeTest5).subscribe();
    this.addEmployee(this.employeeTest6).subscribe();*/
  }


  fetchData() {
    this.employees$ = this.http.get<Employee[]>('/backend/employees', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    });
  }

  addEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>('/backend/employees',employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    });
  }

  getEmployeeById(id: number): Observable<Employee> {
    const url = `/backend/employees/${id}`;
    return this.http.get(url, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    });
  }

  putEmployee(employee: Employee): Observable<Employee> {
    const url = `/backend/employees/${employee.id}`;
    return this.http.put(url, employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    });
  }

  delete(employee: Employee) {
    const url = `/backend/employees/${employee.id}`;
    return this.http.delete(url, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    }).subscribe();
  }

  addQualificationToEmployee(id: number, qualification: Qualification): Observable<Employee> {
    return this.http.post<Employee>(`/backend/employees/${id}`,qualification, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.loginService.getToken()}`)
    });
  }



}
