import { TestBed } from '@angular/core/testing';

import { TokenHolderService } from './token-holder.service';

describe('TokenHolderService', () => {
  let service: TokenHolderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TokenHolderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
