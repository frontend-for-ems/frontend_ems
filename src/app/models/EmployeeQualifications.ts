export class EmployeeQualifications {
  constructor(public id?: number, public lastName?: string, public firstName?: string, public skillSet?: SkillSet[]) {
  }
}

export class SkillSet {
  constructor(public designation?: string) {
  }
}
